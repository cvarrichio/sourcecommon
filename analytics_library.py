def target_encode(train,target):
   categoricals = get_categoricals(train)
   target_encodings = [train[col].replace(train[[col,target]].groupby(col).mean().to_dict()[target]) for col in categoricals]
   train_target_encodings = pd.DataFrame(target_encodings).T
   return train_target_encodings

def prep_data(train): 
    train.columns = map(str.lower, train.columns) 
    train.columns = train.columns.str.replace(r"[^a-zA-Z\d\_]+", "")
    return train

def rename_duplicate_columns(cols):  
    import pandas as pd
    cols = pd.Series(cols)  
    for dup in cols[cols.duplicated()].unique(): 
        cols[cols[cols == dup].index.values.tolist()] = [dup + '_' + str(i) if i != 0 else dup for i in range(sum(cols == dup))]
    return cols


def apply_models (models,data):
    import pandas as pd
    """Runs predictions for each model from models, and then concantenates those results onto the original dataset."""
    predictions = [model.predict(data[[key['name'] for key in model.model_definition['input_features']]]) for model in models]
    combined_matrix = pd.concat([data.reset_index(drop=True)] + predictions,axis=1)
    combined_matrix.columns = rename_duplicate_columns(combined_matrix.columns)
    return combined_matrix



def ensemble_and_predict(X,y_column,train_index,val_index,score):
    
    import xgboost as xgb
    from sklearn.preprocessing import LabelEncoder
    import pandas as pd
    import numpy as np

    X.columns = X.columns.str.replace('<|>','')
    score.columns = score.columns.str.replace('<|>','') 

    y = X[y_column]
    X = X.drop(columns = y_column)

    X = pd.concat([X,X.select_dtypes('object').applymap(hash).astype(int)],axis=1)
    X = X.select_dtypes(['float32','float64','int32','int64','bool'])

    score = pd.concat([score,score.select_dtypes('object').applymap(hash).astype(int)],axis=1)
    score = score.select_dtypes(['float32','float64','int32','int64','bool'])

    label_encoder = LabelEncoder()
    label_encoder = label_encoder.fit(y)
    label_encoded_y = label_encoder.transform(y)

    X_train = X[train_index]
    X_val  = X[val_index]
    y_train = label_encoded_y[train_index]
    y_val = label_encoded_y[val_index]

    xgb_model = xgb.XGBClassifier(objective='multi:softprob',
                      eval_metric = 'merror',
                      verbosity = 1,
                      learning_rate = 0.1,  
                      colsample_bytree = 0.4,
                      subsample = 0.8,
                      reg_alpha = 0.3,
                      max_depth=4, 
                      gamma=10,
                      scale_pos_weight=10, 
                      seed=9
                      )

    xgb_model.fit(X_train,y_train)
    
    #Print model feature importance
    print({k: v for k, v in sorted(xgb_model.get_booster().get_fscore().items(), key=lambda item: item[1],reverse=True)})

    val_predictions = xgb_model.predict(X_val)
    val_predictions_matrix = pd.DataFrame(xgb_model.predict_proba(X_val))
    
    score_predictions_matrix = pd.DataFrame(xgb_model.predict_proba(score[X_train.columns]))

    #xgb_predictions_matrix.columns = label_encoder.inverse_transform(xgb_model.classes_)
    #xgb_predictions_decoded = label_encoder.inverse_transform(xgb_predictions)
    #pd.crosstab(label_encoder.inverse_transform(y_val),xgb_predictions_decoded)

    # from sklearn.metrics import roc_auc_score
    # roc_auc_score((y_val==0)*1,xgb_predictions_matrix['Data Analytics']) 
    # from sklearn.metrics import roc_curve
    # roc_curve((y_val==0)*1,xgb_predictions_matrix['Data Analytics'])
    # lr_fpr, lr_tpr, _ = roc_curve((y_val==0)*1,xgb_predictions_matrix['Data Analytics'])
    # from matplotlib import pyplot
    # pyplot.plot(lr_fpr, lr_tpr, marker='.', label='Logistic')

    
    def remap_precisions(val_predictions,val_actual,score_predictions):
        from sklearn.metrics import precision_recall_curve
        precision, recall, thresholds = precision_recall_curve(val_actual,val_predictions)
        lookups = pd.DataFrame({'thresholds':thresholds,'precision':precision[:-1]}).sort_values('thresholds')
        lookups.precision = np.maximum.accumulate(lookups.precision)
        score_values = pd.DataFrame({'prediction':np.sort(score_predictions.unique().astype('float32'))})
        score_lookups = pd.merge_asof(score_values,lookups,left_on = 'prediction',right_on = 'thresholds').fillna(0) 
        score_lookups.prediction = (score_lookups.prediction * 100000).astype('int')
        score_lookups = score_lookups.drop_duplicates(subset='prediction')
        score_lookups = score_lookups.set_index('prediction')   
        score_predictions_df = pd.DataFrame({'prediction':(score_predictions * 100000).astype('int')})
        score_mapping = score_predictions_df.join(score_lookups,on='prediction')
        score_mapping = score_mapping.fillna(0).reset_index()
        return score_mapping['precision']

    #This could use some improvement
    score_predictions_replaced = pd.DataFrame([remap_precisions(val_predictions_matrix[column_name],(y_val==column_name)*1,score_predictions_matrix[column_name]) for column_name in val_predictions_matrix.columns]).T
    score_predictions_replaced.columns = label_encoder.inverse_transform(xgb_model.classes_)

    return score_predictions_replaced, xgb_model

def plot_roc_curve(actuals,predictions,title):
    from sklearn.metrics import roc_auc_score
    roc_auc = roc_auc_score(actuals,predictions) 
    from sklearn.metrics import roc_curve
    lr_fpr, lr_tpr, _ = roc_curve(actuals,predictions)
    from matplotlib import pyplot
    pyplot.figure()
    pyplot.axis([None, None, 0, 1])
    pyplot.plot(lr_fpr, lr_tpr, marker='.', label='Logistic')
    pyplot.title(title + ' ROC: AUC={0:0.2f} Predictions = {1}'.format(roc_auc,len(actuals)))
    pyplot.xlabel('False Positive Rate')
    pyplot.ylabel('True Positive Rate (Sensitivity)')
    pyplot.show()

def get_prediction_matches(table,field1,value):
    index = table[field1].notnull().to_numpy() & (table['prediction']==value).to_numpy()
    actuals = (table[index][field1]==value)*1
    predictions = table[index]['confidence']
    return(actuals,predictions)

def hvwl_process(func,table,value):
    try:
        index = table.hvwl.notnull().to_numpy() & (table.prediction==value).to_numpy()
        return func(table[index].hvwl==value,table[index].confidence,value)        
    except:
        pass

def plot_pr_curve(actuals,predictions,title):
    from sklearn.metrics import precision_recall_curve
    precision, recall, thresholds = precision_recall_curve(actuals,predictions)
    from sklearn.metrics import auc
    pr_auc = auc(recall, precision)     
    from matplotlib import pyplot
    pyplot.figure()
    pyplot.axis([None, None, 0, 1])
    pyplot.plot(recall, precision, marker='.', label='Logistic')
    pyplot.title(title + ' Precision-Recall: AUC={0:0.2f} Predictions = {1}'.format(pr_auc,len(actuals)))
    pyplot.xlabel('Recall')
    pyplot.ylabel('Precision')
    pyplot.show()

def plot_precision_prediction_curve(actuals,predictions,title):
    from sklearn.metrics import precision_recall_curve
    precision, recall, thresholds = precision_recall_curve(actuals,predictions)
    explained = np.square(precision[1:] - thresholds).sum()
    total = np.square(precision[1:] - thresholds.mean()).sum()
    r2 = 1- explained/total
    from matplotlib import pyplot
    pyplot.figure()
    pyplot.axis([None, None, 0, 1])
    pyplot.plot(thresholds, precision[1:], marker='.', label='Logistic')
    pyplot.title(title + ' Precision-Prediction: R^2={0:0.2f} Predictions = {1}'.format(r2,len(actuals)))
    pyplot.xlabel('Prediction')
    pyplot.ylabel('Precision')
    pyplot.show()
    
    
def get_categoricals(train,limit = .05):
    na_counts = train.isna().sum()
    col_unique_counts = train.apply(lambda x: len(x.unique()))
    categoricals = set(col_unique_counts.where(col_unique_counts/(train.shape[0]-na_counts+1)<limit).dropna().index).intersection(set(train.select_dtypes('O').columns))
    return categoricals

def remove_sparse_values(source_data,target_data,threshold=100):
    results = target_data.copy()
    #threshold = 10 # Anything that occurs less than this will be removed.
    columns = set(get_categoricals(source_data)).intersection(set(get_categoricals(target_data)))
    for column in columns:
        value_counts = source_data[column].value_counts() # Entire DataFrame 
        target_values = target_data[column].unique()
        to_keep_source = value_counts[value_counts > threshold].index
        to_keep_target = list(set(target_values) & set(to_keep_source))
        to_remove = list(set(target_values) - set(to_keep_source))
        replacement_dict = dict(zip(list(to_remove),['Other']*len(to_remove)))
        replacement_dict.update(dict(zip(list(to_keep_target),list(to_keep_target))))
        results[column] = results[column].map(replacement_dict,na_action='ignore')
    return(results)
    ##TOO SLOW
    #value_counts = source_data.select_dtypes('object').stack().value_counts() # Entire DataFrame 
    #target_values = target_data.select_dtypes('object').stack().unique()
    #to_keep = value_counts[value_counts > threshold].index
    #to_remove = list(set(target_values) - set(to_keep))
    #replacement_dict = dict(zip(list(to_remove),['Other']*len(to_remove)))
    #result = target_data.replace(to_remove, 'Other')
    #return target_data

def plot_confidence_hist(confidence,title):
    from matplotlib import pyplot
    pyplot.figure()
    pyplot.hist(confidence, bins='auto')
    pyplot.title(title + ' Confidence Distribution (' + str(confidence.shape[0]) + ' total)')
    pyplot.show()

def proxy_explainer_model(X,y_column):
    import xgboost as xgb
    from sklearn.preprocessing import LabelEncoder
    import pandas as pd
    import numpy as np

    X.columns = X.columns.str.replace('<|>','')

    y = X[y_column]
    X = X.drop(columns = y_column)

    X = pd.concat([X,X.select_dtypes('object').applymap(hash).astype(int)],axis=1)
    X = X.select_dtypes(['float32','float64','int32','int64','bool'])

    label_encoder = LabelEncoder()
    label_encoder = label_encoder.fit(y)
    label_encoded_y = label_encoder.transform(y)

    xgb_model = xgb.XGBClassifier(objective='multi:softprob')

    xgb_model.fit(X,label_encoded_y)

    predictions_df = pd.DataFrame(xgb_model.predict_proba(X))
    
    shap_indices = np.argmax(predictions_df.to_numpy(),axis=1)

    shap_df = shap_importance_for_max_class(xgb_model,X,shap_indices)

    return(shap_df)
    

def shap_importance_for_max_class(model, X, shap_indices):
    import shap
    X = pd.concat([X,X.select_dtypes('object').applymap(hash).astype(int)],axis=1)
    X = X.select_dtypes(['float32','float64','int32','int64','bool'])
    explainer = shap.TreeExplainer(model)
    shap_values = explainer.shap_values(X[model.get_booster().feature_names])    
    shap_array = np.stack([shap_values[shap_indices[x]][x,:] for x in range(len(shap_indices))],axis=0)
    shap_df = pd.DataFrame(shap_array)
    shap_df.columns = model.get_booster().feature_names
    return shap_df

def f1_score_manual(table,field):
    tmp = [get_prediction_matches(table,field,value) for value in table[field].unique()]
    weights = [len(predictions) for actuals,predictions in tmp]
    f1_scores = [f1_score_single(predictions,actuals) if len(predictions) > 0 else 0 for actuals,predictions in tmp]
    weighted_score = (np.nan_to_num(np.array(f1_scores)) * np.array(weights)).sum()/sum(weights)
    return(weighted_score)

def f1_score_single(predictions,actuals):
    from sklearn.metrics import precision_recall_curve
    precision, recall, thresholds = precision_recall_curve(actuals,predictions)
    f1_scores = 2*((precision*recall)/(precision+recall))
    return (f1_scores.max())

def evaluate_performance(score_results):

    score_results = score_results[~score_results.hvwl.isna()]
    print("Size of labeled scoring set: " + str(score_results.shape[0]))
    display(pd.crosstab(score_results[score_results.confidence>.8]['hvwl'],score_results[score_results.confidence>.8]['prediction'],normalize='columns'))
    display(pd.crosstab(score_results[score_results.confidence>.8]['hvwl'],score_results[score_results.confidence>.8]['prediction']))
    print("F1_score for >.8: " + str(f1_score_manual(score_results[score_results.confidence>.8],'hvwl')))
    print("Modified F1_score for >.8: " + str(sum(score_results.confidence > .8)/(score_results.shape[0]) * f1_score_manual(score_results[score_results.confidence>.8],'hvwl')))
    display(pd.crosstab(score_results[score_results.confidence>.9]['hvwl'],score_results[score_results.confidence>.9]['prediction'],normalize='columns'))
    display(pd.crosstab(score_results[score_results.confidence>.9]['hvwl'],score_results[score_results.confidence>.9]['prediction']))
    print("F1_score for >.9: " + str(f1_score_manual(score_results[score_results.confidence>.9],'hvwl')))
    print("Modified F1_score for >.8: " + str(sum(score_results.confidence > .9)/(score_results.shape[0]) * f1_score_manual(score_results[score_results.confidence>.9],'hvwl')))
    display(pd.crosstab(score_results[score_results.confidence>.95]['hvwl'],score_results[score_results.confidence>.95]['prediction']))
    display(pd.crosstab(score_results[score_results.confidence>.95]['hvwl'],score_results[score_results.confidence>.95]['prediction'],normalize='columns'))
    print("F1_score for >.95: " + str(f1_score_manual(score_results[score_results.confidence>.95],'hvwl')))
    print("Modified F1_score for >.95: " + str(sum(score_results.confidence > .95)/(score_results.shape[0]) * f1_score_manual(score_results[score_results.confidence>.95],'hvwl')))

    for value in score_results.hvwl.unique():
        hvwl_process(plot_roc_curve,score_results,value)

    for value in score_results.hvwl.unique():
        hvwl_process(plot_pr_curve,score_results,value)

    for value in score_results.hvwl.unique():
        hvwl_process(plot_precision_prediction_curve,score_results,value)

    for value in score_results.hvwl.unique():
        hvwl_process(plot_confidence_hist,score_results,value)

    f1score = f1_score_manual(score_results,'hvwl')
    logger.info('Aggregate weighted F1 score for model on known values in scoring evaluation: ' + str(f1score))

