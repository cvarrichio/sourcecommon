cityRecurse<-function(data)
{
  
  dataList<-split(data,1:(dim(data)[1]))
  result<-Reduce_pb(combine,dataList)
  return(result)
  
}


combine<-function(data,insert)
{
  dataPlus<-cbind(data,sqrt(unlist(Map(function (a,b) (b-a)^2,insert$x,data$x)) + unlist(Map(function (a,b) (b-a)^2,insert$y,data$y))))
  colnames(dataPlus)[length(colnames(dataPlus))]<-"newDist"
  if(!is.null(insert$previous))
    if(!is.na(insert$previous))
    {
      #print(paste("Blocking previous city ",insert$previous, ' for city ',insert$cityId))
      dataPlus[dataPlus$cityId==insert$previous,"newDist"]<-9999999
    }
  if(!is.null(insert$"next"))
    if(!is.na(insert$"next"))
      dataPlus[dataPlus$cityId==insert$"next","newDist"]<-9999999
  pairs<-c(dataPlus$newDist,0)+c(0,dataPlus$newDist)
  dataPlus<-rbind(c(rep(0,times=(dim(dataPlus)[2]))),dataPlus)
  dataPlus<-cbind(dataPlus,pairs)
  colnames(dataPlus)[length(colnames(dataPlus))]<-"totalDist"
  dataPlus<-cbind(dataPlus,dataPlus$dist-dataPlus$totalDist)
  colnames(dataPlus)[length(colnames(dataPlus))]<-"change"
  #print(max(dataPlus$change))
  index<-which.max(dataPlus$change)
  dataPlus<-rbind(dataPlus,c(rep(0,times=(dim(dataPlus)[2]))))
  insertPlus<-cbind(insert[,-(which(colnames(insert)=='dist'))],dataPlus[(index+1),"newDist"])
  colnames(insertPlus)[length(colnames(insertPlus))]<-"dist"
  #colnames(insertPlus)<-colnames(dataPlus[,1:6])
  dataPlus[index,"dist"]<-dataPlus[index,"newDist"]
  result<-rbind(dataPlus[1:index,colnames(insertPlus)],insertPlus,dataPlus[(index+1):(dim(dataPlus)[1]),colnames(insertPlus)])
  return(result[2:(dim(result)[1]-1),])
}


totalDistance<-function(data)
{
  return(sum(sqrt(unlist(Map(function (a,b) (a-b)^2,data$x,data[2:(dim(data)[1]),"x"])) + unlist(Map(function (a,b) (a-b)^2,data$y,data[2:(dim(data)[1]),"y"])))))
}

binProcess<-function(bins,fun)
{
  lapply(bins,fun)
}

runTSP<-function(cityList,method='farthest_insertion',badPairs)
{
  TSPobj<-TSP(dist(cityList[,c('x','y')]),labels=cityList$cityId)
  aTSPobj<-as.ATSP(TSPobj)
  
  setMax<-function(pairs)
 {
    label1<-as.character(pairs[1])
    label2<-as.character(pairs[2])
    aTSPobj[label1,label2]<-9999999  
    aTSPobj[label2,label1]<-9999999
    print(as.dist(aTSPobj))
  }
  foreach(pairs=iter(badPairs, by='row')) %do% setMax(pairs)

#tour<-solve_TSP(TSPobj,method)
 # return(cityList[labels(tour),])
  return (aTSPobj)
}

fixWorst<-function(data)
{
  worst<-which.max(data$dist)
  fixed<-combine(data[worst,],data[-worst,])
  fixed<-cbind(fixed[,1:3],distances(fixed))
  colnames(fixed)[4]<-"dist"
  return(fixed)
}

insertCity<-function(data,insert)
{
  fixed<-combine(data,insert)
  fixed<-cbind(fixed[,1:3],distances(fixed))
  colnames(fixed)[4]<-"dist"
  #curVal <- get("counter", envir = env)
  #assign("counter", curVal +1 ,envir=env)
  #setTxtProgressBar(get("pb", envir=env), curVal +1)
  return(fixed)
}

optimizeTSP<-function(data,number=100)
{
  worstIndices<-order(data$dist,decreasing=TRUE)[1:number]
  #print(worstIndices)
  worst<-data[worstIndices+1,] #take the city AFTER the worst distances index, as it is the one out of place
  remaining<-data[-(worstIndices+1),-which(colnames(worst)=='dist')]
  remaining<-cbind(remaining,distances(remaining))
  colnames(remaining)[length(colnames(remaining))]<-'dist'
  worstList<-split(worst,1:(dim(worst)[1]))
  #print(worstList)
  result<-Reduce_pb(combine,worstList,init=remaining)
  return(result)
}

totalDistance<-function(data)
{
  return(sum(distances(data)))  
}

distances<-function(data)
{
  dists<-sqrt(unlist(Map(function (a,b) (a-b)^2,data[1:(dim(data)[1]-1),"x"],data[2:(dim(data)[1]),"x"])) + unlist(Map(function (a,b) (a-b)^2,data[1:(dim(data)[1]-1),"y"],data[2:(dim(data)[1]),"y"])))
  return(c(dists,0))
}

reorder<-function(data)
{
  divided<-split(data,rep(1:(dim(data)[1]/6),each=6))
  reorder<-lapply_pb(divided, function (data) data[c(1,3,6,4,2,5),])
  combined<-Reduce_pb(rbind, reorder)
  return(combined)
}

bin<-function(data,binColumns,bins,method="cluster")
{
  fit<-kmeans(data[,binColumns],centers=bins)
  result<-split(data,fit$cluster)
  return(result)
}

nOpt<-function(data,insert)
{
  heads<-head(data,n=-3)
  tails<-tail(data,n=3)
  test1<-rbind(tails,insert)
  test1<-test1[,-which(colnames(test1)=='dist')] #remove dist column
  test1<-cbind(test1,distances(test1))
  colnames(test1)[length(colnames(test1))]<-"dist"
  #print(test1)
  test2<-test1[c(1,3,2,4),-which(colnames(test1)=='dist')]
  test2<-cbind(test2,distances(test2))
  colnames(test2)[length(colnames(test2))]<-"dist"
  #print(test2)
  if((sum(test1$dist))<(sum(test2$dist)))
    result<-test1
  else
      result<-test2
  #print(result)
  return(rbind(heads,result))
}

nOpt2<-function(data,insert,num)
{
  heads<-head(data,n=-(num+1))
  tails<-tail(data,n=(num+1))
  test1<-rbind(tails,insert)
  test1<-test1[,-which(colnames(test1)=='dist')] #remove dist column
  test1<-cbind(test1,distances(test1))
  colnames(test1)[length(colnames(test1))]<-"dist"
  #print(test1)
  end<-dim(test1)[1]
  if(end==4)
    indices<-c(1,3,2,4)
  else
    indices<-c(1,(end-1),3:(end-2),2,end)
  test2<-test1[indices,-which(colnames(test1)=='dist')]
  test2<-cbind(test2,distances(test2))
  colnames(test2)[length(colnames(test2))]<-"dist"
  #print(test2)
  if((((dim(detectBadPairs2(test2))[1])>0) || (sum(test1$dist))<(sum(test2$dist))))
    result<-test1
  else
    result<-test2
  #print(result)
  return(rbind(heads,result))
}

twoOpt<-function(data)
{
  tails<-tail(data,n=-4)
  heads<-head(data,n=4)
  tailsList<-split(tails,1:(dim(tails)[1]))
  result<-Reduce_pb(nOpt,tailsList,init=heads)
  return(result)
}

nOptimizer<-function(data,num=2)
{
  sDist<-totalDistance(data)
  print(paste("Starting length:",sDist))  
  tails<-tail(data,n=-(num+2))
  heads<-head(data,n=(num+2))
  tailsList<-split(tails,1:(dim(tails)[1]))
  result<-Reduce_pb(function(a,b) nOpt2 (a,b,num),tailsList,init=heads)
  eDist<-totalDistance(result)
  print(paste("Ending length:",eDist))  
  change<-sDist-eDist
  print(paste("Num=",num,"Improvement:",change))
  return(result)
}

badPairs<-function(data)
{
  badpairs<-cbind(data$cityId,c(NA,data[-(dim(data)[1]),"cityId"]),c(data[-1,"cityId"],NA))
  full<-cbind(badpairs,data[,c("x","y","dist")])
  colnames(full)[1:3]<-c("cityId","previous","next")
  return(full)
}


detectBadPairs<-function(first,second)
{
  fBadPairs<-badPairs(first)
  sBadPairs<-badPairs(second)
  colnames(sBadPairs)<-paste(colnames(sBadPairs),'1',sep='.')
  cBadPairs<-cbind(fBadPairs,sBadPairs[match(fBadPairs$cityId,sBadPairs$cityId),])
  result<-cBadPairs[((cBadPairs$previous==cBadPairs$previous.1) | (cBadPairs$previous==cBadPairs$next.1) | (cBadPairs$"next"==cBadPairs$previous.1) | (cBadPairs$"next"==cBadPairs$next.1)),]
  return(result[!is.na(result$cityId),])
}

detectBadPairs2<-function(data)
{
  pairMatch<-cbind(data,c(0,head(data$cityId,-1)))
  colnames(pairMatch)[length(colnames(pairMatch))]<-'head'
  pairMatch<-cbind(pairMatch,c(tail(data$cityId,-1),0))
  colnames(pairMatch)[length(colnames(pairMatch))]<-'tail'
  badMatches<-pairMatch[pairMatch$previous==pairMatch$head | pairMatch$previous==pairMatch$tail | pairMatch$"next"==pairMatch$head | pairMatch$"next"==pairMatch$tail,]
  return(badMatches[!is.na(badMatches$cityId),])
}

updateBadPairs<-function(first,second)
{
  fBadPairs<-badPairs(first)
  sBadPairs<-badPairs(second)
  colnames(sBadPairs)<-paste(colnames(sBadPairs),'1',sep='.')
  cBadPairs<-cbind(sBadPairs,fBadPairs[match(sBadPairs$cityId,fBadPairs$cityId),])
  result<-cBadPairs[,c('cityId','previous','next','x','y','dist')]
  return(result)
}

plotTSP<-function(data)
{
  (function (data) plot(x=data$x,y=data$y,cex=0.1)) (Reduce(rbind,data))
  sapply(1:length(data),function(a) lines(data[[a]]$x,data[[a]]$y,col=(30*a)))
}


