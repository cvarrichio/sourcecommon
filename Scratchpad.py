https://stackoverflow.com/questions/2827623/python-create-object-and-add-attributes-to-it

obj =type('', (dict,), {})
obj.a = 1

MyObject = type('MyObject', (object,), {})
obj = MyObject()
obj.a = 1

class Object(object):
    pass

obj = Object()
obj.a = 1


a = {}
a['a'] =1 #Cannot use periods in this case

obj = lambda:None
obj.a = 1

#If we want something interchangeable with a dictionary AND
#can use the dot convention, making a class Object that extends
#dictionary is the best solution

class Object(dict):
    def __init__(self,**kw):
        dict.__init__(self,kw)
        self.__dict__ = self

class Object(dict):
    def __init__(self,**kw):
        dict.__init__(self,kw)
        self.__dict__.update(kw)

{k: v for d in dicts for k, v in d.items()} #

